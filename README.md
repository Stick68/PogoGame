## Protocle d'utilisation 

---

### Windows :
- ### Installation :
    - Installer ([Python 3] (https://www.python.org/downloads/windows/)) ou supérieur
    - Installer les modules nécessaires :
    `pip install -r requirements.txt`
- ### Exécution
    - Depuis le répertoire "sources" : `main1.py`

---