import pygame
import sys
from button import Button
import subprocess

# Initialisation de Pygame
pygame.init()

# Taille fenetre
screen_width = 1280
screen_height = 720
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Menu")

# Chargement du BackGround
background = pygame.image.load("Menu/assets/Background.png")
# On donne les parametres du BackGround
background = pygame.transform.scale(background, (screen_width, screen_height))

cook=pygame.image.load("Menu/assets/pogocook_ie.png")
cook=pygame.transform.scale(cook,(200,200))

died=pygame.image.load("Menu/assets/mort.png")
died=pygame.transform.scale(died,(300,300))
# Fonction pour la police
def get_font(size):
    return pygame.font.Font("Menu/assets/font.ttf", size)

#fonctions qui sont appelés quand on cliqueras sur certains boutons pour ouvrir un programme pygame
def launch_jeux1():
    subprocess.Popen(["python", "Jeu1/Launcher.py"])
def launch_jeux2():
    subprocess.Popen(["python", "Jeu2/Jeu2.py"])

# Fonction jeux 
def play(prenom):
    while True:
        play_mouse_pose = pygame.mouse.get_pos()
        screen.fill("black")
        play_txt = get_font(25).render("Bienvenue " + prenom + ", je suis Pogo", True, "White")
        play_rekt = play_txt.get_rect(center = (500, 150))
        screen.blit(play_txt, play_rekt)
        Pogo_mascotte_img = pygame.image.load("Menu/assets/pogo.png")
        screen.blit(Pogo_mascotte_img, (530, 300))

        # Ajout des boutons "Jeux 1" et "Jeux 2"
        Bouton1 = Button(image = None, pos = (300, 500),
                         text_input = "Jeu 1", font = get_font(30), base_color = "White", hovering_color = "Green")
        Bouton2 = Button(image = None, pos = (900, 500),
                         text_input = "Jeu 2", font = get_font(30), base_color = "White", hovering_color = "Green")

        Bouton1.changeColor(play_mouse_pose)
        Bouton1.update(screen)
        Bouton2.changeColor(play_mouse_pose)
        Bouton2.update(screen)

        play_back = Button(image = None, pos = (640, 660),
                           text_input = "BACK", font = get_font(75), base_color = "White", hovering_color = "Green")

        play_back.changeColor(play_mouse_pose)
        play_back.update(screen)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if Bouton1.checkForInput(play_mouse_pose):
                    # Lancer le j
                    launch_jeux1()
                if Bouton2.checkForInput(play_mouse_pose):
                    launch_jeux2()
                    # Faire quelque chose lorsque "Jeux 2" est cliqué
                elif play_back.checkForInput(play_mouse_pose):
                    main_menu()

        pygame.display.update()



# Fonction pour obtenir l'entrée utilisateur
def get_input(prompt, font):
    input_box = ""
    input_active = True
    max_chars = 15

    while input_active:
        screen.fill((0, 0, 0))  # Efface l'écran avant de dessiner la boîte de texte

        input_text = font.render(prompt + input_box, True, (255, 255, 255))
        input_rect = input_text.get_rect(topleft = (150, 300))
        screen.blit(input_text, input_rect)

        pygame.display.flip()

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    input_active = False
                elif event.key == pygame.K_BACKSPACE:
                    input_box = input_box[:-1]
                elif len(input_box) < max_chars:  # Vérifie la longueur maximale du prénom
                    input_box += event.unicode
                    input_box = input_box[:max_chars]  # Limite longueur texte à max_chars

    return input_box

# Fonction pour le menu principal
def main_menu():
    while True:        #jusqu' à False
        screen.blit(background, (0, 0))
        screen.blit(cook,(150,350))
        screen.blit(died,(900,350))
        

        menu_mouse_pose = pygame.mouse.get_pos()

        menu_txt = get_font(100).render("POGO GAMES", True, "#FFB833")
        Menu_rekt = menu_txt.get_rect(center=(640, 150))

        play_bouton = Button(image = pygame.image.load("Menu/assets/Play Rect.png"), pos = (640, 350),
                             text_input = "PLAY", font = get_font(75), base_color = "#d7fcd4", hovering_color = "White")
        quit_bouton = Button(image=pygame.image.load("Menu/assets/Quit Rect.png"), pos=(640, 520),
                             text_input="QUIT", font=get_font(75), base_color="#d7fcd4", hovering_color="White")

        screen.blit(menu_txt, Menu_rekt)

        for button in [play_bouton, quit_bouton]:
            button.changeColor(menu_mouse_pose)
            button.update(screen)

        pygame.display.update()

        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if play_bouton.checkForInput(menu_mouse_pose):
                    prenom = get_input("Entrez votre prénom : ", get_font(30))
                    play(prenom)
                elif quit_bouton.checkForInput(menu_mouse_pose):
                    pygame.quit()
                    sys.exit()

        pygame.display.update()

# Lancement du menu principal
main_menu()

