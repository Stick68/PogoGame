import pygame
from pygame.locals import *
from Player import *
from Enemies import *


pygame.init()

#taille fenetre
window_width = 1200
window_height = 680
running = True
screen = pygame.display.set_mode((window_width, window_height))

#Les class Enemies et Player importés des autres fichiers 
player = Player()
enemies = Enemies()

#importation et parametre du bg
background = pygame.image.load("Jeu1/assets/space.jpg")
background = pygame.transform.scale(background, (window_width, window_height))


# Taille et image pour les fleches directionnelles (info)
control_top=pygame.image.load("Jeu1/assets/KeyboardButtonsDir_up.png")
control_bottom=pygame.image.load("Jeu1/assets/KeyboardButtonsDir_down.png")
control_left=pygame.image.load("Jeu1/assets/KeyboardButtonsDir_left.png")
control_right=pygame.image.load("Jeu1/assets/KeyboardButtonsDir_right.png")
control_top=pygame.transform.scale(control_top,(50,50))
control_bottom=pygame.transform.scale(control_bottom,(50,50))
control_left=pygame.transform.scale(control_left,(50,50))
control_right=pygame.transform.scale(control_right,(50,50))





clock = pygame.time.Clock()

#def du font pour le texte 
fontP = pygame.font.Font("Menu/assets/font.ttf", 30)  # Police pour afficher "Game Over"
game_over_text = fontP.render("Game Over ! Appuyer sur ECHAP", True, (255, 0, 0))  # Texte "Game Over" rouge

#systeme pour fermer le programme
while running:
    clock.tick(60)
    
    for event in pygame.event.get():
        if pygame.key.get_pressed()[K_ESCAPE]:
            running = False
        if event.type == pygame.QUIT:
            running = False
    
    player.update(enemies)
    enemies.update()

# Affichage pour les fleches directionnelles sur l'ecran à des coos x,y
    screen.blit(background, (0, 0))
    screen.blit(control_bottom,(550,600))
    screen.blit(control_top,(550,550))
    screen.blit(control_left,(500,600))
    screen.blit(control_right,(600,600))


    player.display(screen)
    enemies.display(screen)
    
    if player.hp <= 0:
        screen.blit(game_over_text, (window_width // 2 - game_over_text.get_width() // 2, window_height // 2 - game_over_text.get_height() // 2))

    pygame.display.update()

pygame.quit()  # Fermeture de Pygame lorsque la boucle se termine

