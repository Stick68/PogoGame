import pygame
import random
import sys
from pygame.locals import *

# Initialisation de Pygame
pygame.init()

# Couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)
VERT = (0, 255, 0)
BLEU = (0, 0, 255)
VIOLET = (114, 9, 183)
TRANSPARENT = (0, 0, 0, 0)

# Constantes

FPS = 60

# Taille de la fenêtre
largeur_fenetre, hauteur_fenetre = 1000, 600
fenetre = pygame.display.set_mode((largeur_fenetre, hauteur_fenetre))
pygame.display.set_caption("Tamagotch")

arriere_plan = pygame.image.load("Jeu2/assets2/Background.png")
arriere_plan = pygame.transform.scale(arriere_plan, (largeur_fenetre, hauteur_fenetre))
animal = pygame.image.load("Jeu2/assets2/Pogo.png")


def obtenir_police(taille):
    return pygame.font.Font("Jeu2/assets2/font.ttf", taille)

# Fonction pour dessiner le texte sur l'écran
def texte(surface, text, font, color, x, y):
    surface_texte = obtenir_police(15).render(text, True, color)
    texte_rect = surface_texte.get_rect()
    texte_rect.midtop = (x, y)
    surface.blit(surface_texte, texte_rect)

def barre_progression(surface, color, x, y, width, height, progress):
    # Dessiner le rectangle noir
    pygame.draw.rect(surface, NOIR, (x - 1, y - 1, width + 2, height + 2), border_radius=5)
    # Dessiner la barre de progression
    pygame.draw.rect(surface, color, (x, y, width * progress, height), border_radius=5)

# Définition de la classe Tamagotchi
class Tamagotchi(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((50, 50))
        self.image.fill(BLANC)
        self.rect = self.image.get_rect(center = (largeur_fenetre // 2, hauteur_fenetre // 2))
        self.faim = 0
        self.joie = 100
        self.energie = 100
        self.en_vie = True

    def update(self):
        if self.en_vie:
            self.faim += 0.1
            self.joie -= 0.085
            self.energie -= 0.1
            self.faim = min(100, max(0, self.faim))
            self.joie = min(100, max(0, self.joie))
            self.energie = min(100, max(0, self.energie))
            if self.faim >= 100 or self.joie <= 0 or self.energie <= 0:
                self.en_vie = False

    def nourrir(self):
        if self.en_vie:
            self.faim -= 10
            self.faim = max(0, self.faim)
            self.joie = min(100, max(0, self.joie))

    def jouer(self):
        if self.en_vie:
            self.joie += 10
            self.energie -= 5
            self.faim += 5
            self.joie = min(100, max(0, self.joie))
            self.energie = min(100, max(0, self.energie))
        
    def dormir(self):
        if self.en_vie:
            self.energie += random.randint(10,30)
            self.energie = min(100, max(0, self.energie))

# Fonction principale du jeu
def jeu():
    tamagotchi = Tamagotchi()
    clock = pygame.time.Clock()
    police = obtenir_police(10)

    # Boutons
    bouton_nourrir = pygame.Rect(450, 400, 120, 30)
    bouton_jouer = pygame.Rect(300, 400, 120, 30)
    bouton_dormir = pygame.Rect(600, 400, 120, 30)

    # Boucle principale du jeu
    while True:

        # Effacer l'écran avec l'image de fond
        fenetre.blit(arriere_plan, (0, 0))

        # Prendre les dimensions de l'image de l'animal
        animal_rect = animal.get_rect()
        animal_largeur, animal_hauteur = animal_rect.width, animal_rect.height

        # Calculer sa position pour le centrer au milieu de la fenêtre
        animal_x = largeur_fenetre // 2 - animal_largeur // 2
        animal_y = hauteur_fenetre // 2 - animal_hauteur // 2

        # Afficher l'animal sur l'écran
        fenetre.blit(animal, (animal_x, animal_y))
        
        # Quitter Pygame et Sys si on quitte le jeu, détecter un clic sur l'un des boutons 
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    pos_souris = event.pos
                    if bouton_nourrir.collidepoint(pos_souris):
                        tamagotchi.nourrir()
                    elif bouton_jouer.collidepoint(pos_souris):
                        tamagotchi.jouer()
                    elif bouton_dormir.collidepoint(pos_souris):
                            tamagotchi.dormir()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()

        
        # Mise à jour de l'état du Tamagotchi
        tamagotchi.update()

        # Dessiner les barres d'état avec bande noire et coins arrondis
        pygame.draw.rect(fenetre, NOIR, (10, 10, 300, 20), border_radius = 5)
        pygame.draw.rect(fenetre, NOIR, (10, 40, 300, 20), border_radius = 5)
        pygame.draw.rect(fenetre, NOIR, (10, 70, 300, 20), border_radius = 5)
        barre_progression(fenetre, BLEU, 10, 10, 300, 20, tamagotchi.energie / 100)
        barre_progression(fenetre, VERT, 10, 40, 300, 20, tamagotchi.joie / 100)
        barre_progression(fenetre, ROUGE, 10, 70, 300, 20, tamagotchi.faim / 100)
        
        bouton_surface = pygame.Surface((120, 30))

        # Dessiner les boutons
        pygame.draw.rect(bouton_surface, TRANSPARENT, bouton_nourrir)
        texte(fenetre, "Nourrir", obtenir_police(20), (0, 0, 0), 515, 407)
        pygame.draw.rect(bouton_surface, TRANSPARENT, bouton_jouer)
        texte(fenetre, "Jouer", obtenir_police(20), (0, 0, 0), 360, 407)
        pygame.draw.rect(bouton_surface, TRANSPARENT, bouton_dormir)
        texte(fenetre, "Dormir", obtenir_police(20), (0, 0, 0), 660, 407)
        pygame.draw.rect(fenetre, NOIR, bouton_nourrir, 2, border_radius = 3)  # 2 représente l'épaisseur de la bordure
        pygame.draw.rect(fenetre, NOIR, bouton_jouer, 2, border_radius = 3)
        pygame.draw.rect(fenetre, NOIR, bouton_dormir, 2, border_radius = 3)

        # Afficher le texte
        texte(fenetre, "Energie", police, BLANC, 95, 13)
        texte(fenetre, "Joie", police, BLANC, 70, 43)
        texte(fenetre, "Faim", police, BLANC, 70, 73)

        # Afficher "Game Over" si Pogo est mort
        if not tamagotchi.en_vie:
            text = obtenir_police(30).render("Game Over ! Appuyer sur ECHAP", True,"green")
            fenetre.blit(text, (largeur_fenetre // 2 - 470, hauteur_fenetre // 2))
            
            

        # Mettre à jour l'affichage
        pygame.display.flip()

        # Limiter le FPS
        clock.tick(FPS)

if __name__ == "__main__":
    jeu()
        
pygame.quit()